<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Imports\dataImport;
use Maatwebsite\Excel\Importer;
use Validator;
use XML;
use XMLWriter;

class DataController extends Controller
{
    //
    public function importForm(){
        return view ('import-form');
    }

    public function import (Request $request)
    {
        $validator=Validator::make($request->all(),[
            'file'=>'required|mimes:csv,txt,xml'
        ]);
        
        if (($validator->passes()) && ($request->file->getClientOriginalExtension() == 'csv')) 
        {
           
            Excel::import(new dataImport , $request->file);
          
            return redirect()->back()->with(['success'=>'Fichier CSV à été bien importer ']);
        
        }elseif (($validator->passes()) && ($request->file->getClientOriginalExtension() == 'xml')) 
        {
        
         
        //    XML::import($request->file)
            return redirect()->back()->with (['success'=>'Fichier XML à été bien importer ']);
        
        } else 
        {
            return redirect()->back()->with(['errors'=>'Vous devez choisir un type de fichier CSV ou xml']);
        }
       /* Excel::import(new dataImport , $request->file);
        return "Donnée a été bien imorter";*/
     

    } 
}
