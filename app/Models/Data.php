<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Facades\DB;
use Illuminate\Support\Facades\DB as FacadesDB;

class Data extends Model
{
    use HasFactory;
    protected $table ="data";
    protected $fillable=['nom','valeur'];
    
    public static function getData(){
        $records=DB::table('data')->select('id','nom','valeur');
        return $records;
    } 

}
