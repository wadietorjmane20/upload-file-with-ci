<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import File CSV</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('style.css') }}"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
</head>
<body>

    <section style="padding-top: 60px;">
    <h2 style="color:white; margin-left:25%;margin-top:5%;font-family: 'Brush Script MT', cursive;font-size:50px;font-weight:bold">Open Data</h2>
    <div class="contaner form"  >
    <div class="row">
    <h1 for="title"> Choisissez votre Fichier CSV</h1>
    <div class="col-md-6 file">
        

                    <form method="POST" enctype="multipart/form-data" action="{{route('data.import')}}">
                        @csrf
                        @if(session('errors'))
                        <div class="alert alert-danger">
                        {{session('errors')}} 
                            </div>
                        @endif
                        @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}} 
                            </div> 
                        @endif
                            <div class="form-group">
                              
                                <input type="file" name="file" class="form-control transparent-input"/>
                            </div>
                            <button type="submit" class="btn btn-primary button"> Télécharger</button>
                    </form>

               
            
        
    </div>
    </div>
    </div>
    <footer class="text-center" style="margin-top:140px">
    <div class="text-center"> <a href="https://tickets.workey.store/tickets/app/#/tickets/tickets/0"><img src="image\user.png"width="70px"height="70px"></a></div>
        <a href="https://tickets.workey.store/tickets/app/#/tickets/tickets/0"style="text-decoration:none;color:white">Contacter nous </a>
    </footer>
    </section>
   
</body>
</html>